Forumly
============

Forumly is an open source simple anonymous forum.

Anyone can post, but only admins can delete and update threads and replies.

Default admin login is 'admin' with the password '123123'.