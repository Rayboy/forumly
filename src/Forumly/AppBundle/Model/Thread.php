<?php

namespace Forumly\AppBundle\Model;

use Forumly\AppBundle\Model\om\BaseThread;

class Thread extends BaseThread
{
    public function preInsert(\PropelPDO $con = null)
    {
        $this->setCreatedAt(time());
        return true;
    }
}
