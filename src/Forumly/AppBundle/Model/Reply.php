<?php

namespace Forumly\AppBundle\Model;

use Forumly\AppBundle\Model\om\BaseReply;

class Reply extends BaseReply
{
    public function preInsert(\PropelPDO $con = null)
    {
        $this->setCreatedAt(time());
        return true;
    }
}
