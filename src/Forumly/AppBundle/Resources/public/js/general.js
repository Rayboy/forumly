$( document ).ready(function() {
    $( '.flash-close' ).click(function() {
        $(this).parent().parent().slideUp();
    } );
} );

function deleteThread(id, viewPage) {
    if (confirm('Are you sure you want to delete this thread?')) {
        $.ajax({
            url: Routing.generate('thread_delete', { id: id }),
            method: 'DELETE'
        }).done(function() {
            if (viewPage) {
                window.location = Routing.generate('home');
            } else {
                $( '#thread_' + id ).slideUp();
            }
        }).fail(function() {
            alert('Failed to delete thread!');
        });
    }
}

function deleteReply(id) {
    if (confirm('Are you sure you want to delete this reply?')) {
        $.ajax({
            url: Routing.generate('reply_delete', { id: id }),
            method: 'DELETE'
        }).done(function() {
            $( '#reply_' + id ).slideUp();
        }).fail(function() {
            alert('Failed to delete reply!');
        });
    }
}
