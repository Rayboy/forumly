<?php
namespace Forumly\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ThreadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', null, array(
            'required' => true
        ));
        $builder->add('body', null, array(
            'required' => true,
            'attr' => array('rows' => '15')
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Forumly\AppBundle\Model\Thread',
        ));
    }

    public function getName()
    {
        return 'thread';
    }
}