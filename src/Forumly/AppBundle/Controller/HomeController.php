<?php

namespace Forumly\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Forumly\AppBundle\Model\ThreadQuery;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function indexAction()
    {
        $threads = ThreadQuery::create()
            ->orderByCreatedAt('desc')
            ->find();
        return array('threads' => $threads);
    }

    /**
     * @Route("/admin/login", name="admin_login")
     */
    public function adminLoginAction()
    {
        return new RedirectResponse($this->generateUrl('home'));
    }

    /**
     * @Route("/admin/logout", name="admin_logout")
     */
    public function adminLogoutAction()
    {
        return new RedirectResponse($this->generateUrl('home'));
    }

}
