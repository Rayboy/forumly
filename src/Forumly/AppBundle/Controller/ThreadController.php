<?php

namespace Forumly\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Forumly\AppBundle\Model\Thread;
use Forumly\AppBundle\Form\Type\ThreadType;
use Forumly\AppBundle\Model\ThreadQuery;

use Forumly\AppBundle\Model\ReplyQuery;

class ThreadController extends Controller
{
    /**
     * @Route("/thread/view/{id}", name="thread_view")
     * @Template("ForumlyAppBundle:Thread:view.html.twig")
     */
    public function threadViewAction($id)
    {
        $thread = ThreadQuery::create()
            ->findPK($id);

        $replies = ReplyQuery::create()
            ->filterByThreadId($id)
            ->orderByCreatedAt('desc')
            ->find();
        return array('thread' => $thread, 'replies' => $replies);
    }

    /**
     * @Route("/thread/post/{id}", name="thread_post")
     */
    public function threadPostAction($id = null)
    {
        $thread = new Thread();
        if ($id) {
            if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                throw new AccessDeniedException();
            }

            $thread = ThreadQuery::create()
                        ->findPK($id);
        }

        $form = $this->createForm(new ThreadType(), $thread);

        $request = $this->getRequest();
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $thread->save();

                if ($id) {
                    $request->getSession()->getFlashBag()->add(
                        'success',
                        'Thread successfully updated!'
                    );
                } else {
                    $request->getSession()->getFlashBag()->add(
                        'success',
                        'Thread successfully created!'
                    );
                }

                return $this->redirect($this->generateUrl('thread_view', array('id' => $thread->getId())));
            } else {
                $request->getSession()->getFlashBag()->add(
                    'failure',
                    'Invalid thread!'
                );
            }
        }

        return $this->render('ForumlyAppBundle:Thread:post.html.twig', array(
            'form' => $form->createView(),
            'thread' => $thread
        ));
    }

    /**
     * @Route("/thread/delete/{id}", name="thread_delete")
     * @Method({"DELETE","HEAD"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function threadDeleteAction($id)
    {
        $thread = ThreadQuery::create()
            ->findPK($id);

        $replies = ReplyQuery::create()
            ->filterByThreadId($id)
            ->find();

        //Delete replies before thread to avoid foreign key issues.
        foreach ($replies as &$reply) {
            $reply->delete();
        }

        $thread->delete();

        return new JsonResponse(array('success' => true));
    }
}
