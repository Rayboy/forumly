<?php

namespace Forumly\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

use Forumly\AppBundle\Model\Reply;
use Forumly\AppBundle\Model\ReplyQuery;
use Forumly\AppBundle\Form\Type\ReplyType;

class ReplyController extends Controller
{
    /**
     * @Route("/thread/{id}/reply", name="reply_post")
     */
    public function replyPostAction($id)
    {
        $reply = new Reply();
        $form = $this->createForm(new ReplyType(), $reply);

        $request = $this->getRequest();
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $reply->setThreadId($id);
                $reply->save();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    'Reply successfully posted!'
                );

                return $this->redirect($this->generateUrl('thread_view', array('id' => $id)));
            } else {
                $request->getSession()->getFlashBag()->add(
                    'failure',
                    'Invalid reply!'
                );
            }
        }

        return $this->render('ForumlyAppBundle:Reply:post.html.twig', array(
            'id' => $id,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/reply/delete/{id}", name="reply_delete")
     * @Method({"DELETE","HEAD"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function replyDeleteAction($id)
    {
        $reply = ReplyQuery::create()
            ->findPK($id);

        $reply->delete();

        return new JsonResponse(array('success' => true));
    }
}
